# canvas-data-aacc

The canvas-data-aacc project is a somewhat direct translation of the Harvard canvas-data-aws project to use Terraform, and a simple deployment package build process. At current we're still using Harvard's lambdas, but a replacement for that is in the works as well.

## Module Prerequisites

Before applying this module you will need to build the deployment package, and define the necessary variables. To build the deployment package:

    $ cd external
    $ ./deployment_package.sh

This builds the deployment package at `external/deployment_package.zip`.  Next, open `terraform.tfvars` and define 3 variables. `canvas_data_api_key`, and `canvas_data_api_secret` - the Canvas data portal API key and secret, and `notification_email` - the email address at which you'd like to receive notifications when a sync completes successfully.

For example, assuming your key is `ABCD` and your secret is `1234`, your `terraform.tfvars` file could look like this:

```tf
canvas_data_api_key    = "ABCD"
canvas_data_api_secret = "1234"
notification_email     = "someone@somewhere.edu"
```

Additionally, if you'd like to store your keys in AWS Secrets Manager, you can set `use_secrets_manager` to `true`:

```tf
use_secrets_manager = true
```

## Applying the Module

It should be noted at this point that setting up Terraform is outside of the scope of this document. You will need to supply your own AWS API credentials and provide them in a way that the `terraform-provider-aws` module accepts (usually environment variable, or credentials file.) Once you've done this, you should be able to run:

    $ terraform apply

    Terraform used the selected providers to generate the following execution plan. Resource actions are indicated with the following
    symbols:
    *details of types of actions to apply removed*

    Terraform will perform the following actions:

    *details of actions to apply removed*

    Plan: x to add, 0 to change, 0 to destroy.

    Do you want to perform these actions?
      Terraform will perform the actions described above.
      Only 'yes' will be accepted to approve.

      Enter a value: yes

      *details of actions taken removed*

        Apply complete! Resources: x added, 0 changed, 0 destroyed.

Once the changes have been applied, and the final infrastructure is in place, a sync should be attempted every morning at 2 AM. To test the process, simply run a test on the `canvas-data-lambda-sync-function` (by default) lambda. There are no requirements for the input data, so passing empty/test data should be fine.
