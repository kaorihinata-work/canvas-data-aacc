# canvas_data_api_key (variable) {{{
variable "canvas_data_api_key" {
  description = "The Canvas Data API key."
  type        = string
}
# }}}
# canvas_data_api_secret (variable) {{{
variable "canvas_data_api_secret" {
  description = "The Canvas Data API secret."
  type        = string
}
# }}}
# canvas_data_secret_name (variable) {{{
variable "canvas_data_secret_name" {
  description = "The name of the Secrets Manager secret."
  type        = string
}
# }}}
