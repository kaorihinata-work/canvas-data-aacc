# credentials_in_secrets_manager
#
# Optionally store the Canvas Data API key and secret in Secrets Manager.

locals {
  credentials = {
    api_key    = var.canvas_data_api_key
    api_secret = var.canvas_data_api_secret
  }
}

# Set up Secrets Manager. {{{
resource "aws_secretsmanager_secret" "secret" {
  name = var.canvas_data_secret_name
}
resource "aws_secretsmanager_secret_version" "credentials_version" {
  secret_id     = aws_secretsmanager_secret.secret.id
  secret_string = jsonencode(local.credentials)
}
data "aws_iam_policy_document" "policy_document" {
  statement {
    actions = ["secretsmanager:GetSecretValue"]
    resources = [aws_secretsmanager_secret.secret.id]
  }
}
# }}}
