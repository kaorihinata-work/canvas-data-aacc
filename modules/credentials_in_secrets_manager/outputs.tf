# api_sm_id (output) {{{
output "api_sm_id" {
  description = "The secret ID to pass to the sync lambda."
  value       = aws_secretsmanager_secret.secret.id
}
# }}}
# policy_document (output) {{{
output "policy_document" {
  description = "A policy document providing access to the Secret."
  value       = data.aws_iam_policy_document.policy_document.json
}
# }}}
