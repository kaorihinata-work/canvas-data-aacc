# Variables for common options.
# canvas_data_api_key (variable) {{{
variable "canvas_data_api_key" {
  description = "The API key for the Canvas Data API."
  type        = string
}
# }}}
# canvas_data_api_secret (variable) {{{
variable "canvas_data_api_secret" {
  description = "The API secret for the Canvas Data API."
  type        = string
}
# }}}
# notification_email (variable) {{{
variable "notification_email" {
  description = "The SNS endpoint (email) for sync notifications."
  type        = string
}
# }}}
# use_secrets_manager (variable) {{{
variable "use_secrets_manager" {
  description = "Optionally use Secrets Manager to store API credentials."
  type        = bool
  default     = false
}
# }}}

# Advanced variables for changing resource names.
# canvas_data_eventbridge_sync_trig (variable) {{{
variable "canvas_data_eventbridge_sync_trig" {
  description = "The name of the EventBridge sync trigger."
  type        = string
  default     = "canvas-data-eventbridge-sync-trigger"
}
# }}}
# canvas_data_glue_catalog_database (variable) {{{
variable "canvas_data_glue_catalog_database" {
  description = "The name of the Glue Catalog \"database\"."
  type        = string
  default     = "canvas-data-glue-catalog-database"
}
# }}}
# canvas_data_iam_role_fetch (variable) {{{
variable "canvas_data_iam_role_fetch" {
  description = "The role for the Canvas Data fetch function."
  type        = string
  default     = "canvas-data-iam-fetch-role"
}
# }}}
# canvas_data_iam_role_sync (variable) {{{
variable "canvas_data_iam_role_sync" {
  description = "The role for the Canvas Data sync function."
  type        = string
  default     = "canvas-data-iam-sync-role"
}
# }}}
# canvas_data_iam_group_policy (variable) {{{
variable "canvas_data_iam_group_policy" {
  description = "The name of the IAM Group policy for researchers."
  type        = string
  default     = "canvas-data-iam-group-policy-for-athena-access"
}
# }}}
# canvas_data_iam_group (variable) {{{
variable "canvas_data_iam_group" {
  description = "The name of the IAM Group for researchers."
  type        = string
  default     = "canvas-data-iam-group-for-athena-access"
}
# }}}
# canvas_data_lambda_perm_sync_trig (variable) {{{
variable "canvas_data_lambda_perm_sync_trig" {
  description = "The name of the Lambda Permission for the sync trigger."
  type        = string
  default     = "canvas-data-lambda-permission-sync-trigger"
}
# }}}
# canvas_data_lambda_fetch_func (variable) {{{
variable "canvas_data_lambda_fetch_func" {
  description = "The name of the Canvas Data fetch function."
  type        = string
  default     = "canvas-data-lambda-fetch-function"
}
# }}}
# canvas_data_lambda_sync_func (variable) {{{
variable "canvas_data_lambda_sync_func" {
  description = "The name of the Canvas Data sync function."
  type        = string
  default     = "canvas-data-lambda-sync-function"
}
# }}}
# canvas_data_s3_bucket (variable) {{{
variable "canvas_data_s3_bucket" {
  description = "The S3 bucket that holds Canvas Data archives."
  type        = string
  default     = "canvas-data-s3-canvas-data-bucket"
}
# }}}
# canvas_data_secret_name (variable) {{{
variable "canvas_data_secret_name" {
  description = "The name of the Secrets Manager secret."
  type        = string
  default     = "canvas-data-secret-name"
}
# }}}
# canvas_data_sns_topic (variable) {{{
variable "canvas_data_sns_topic" {
  description = "The SNS topic for sync notifications."
  type        = string
  default     = "canvas-data-sns-sync-topic"
}
# }}}
# deployment_s3_bucket (variable) {{{
variable "deployment_s3_bucket" {
  description = "The S3 bucket that holds deployment packages."
  type        = string
  default     = "canvas-data-s3-deployment-bucket"
}
# }}}
# deployment_s3_bucket_object_key (variable) {{{
variable "deployment_s3_bucket_object_key" {
  description = "The key to use when uploading deployment_package."
  type        = string
  default     = "deployment-package.zip"
}
# }}}
# query_results_s3_bucket (variable) {{{
variable "query_results_s3_bucket" {
  description = "The S3 bucket that holds query results (for Athena.)"
  type        = string
  default     = "canvas-data-s3-query-results-bucket"
}
# }}}
