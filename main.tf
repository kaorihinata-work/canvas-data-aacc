# canvas-data-aacc
#
# Provision the infrastructure necessary to create a data lake in AWS using
# Canvas Data archives.

# Needed for account ID and region name. {{{
data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
locals {
  account_id = data.aws_caller_identity.current.account_id
  region     = data.aws_region.current.name
}
# }}}
# Set up the S3 bucket for deployment packages. {{{
resource "aws_s3_bucket" "deployment" {
  acl           = "private"
  bucket        = var.deployment_s3_bucket
  force_destroy = true
}
resource "aws_s3_bucket_object" "deployment_package" {
  bucket = aws_s3_bucket.deployment.id
  etag   = filemd5("${path.module}/external/deployment_package.zip")
  key    = var.deployment_s3_bucket_object_key
  source = "${path.module}/external/deployment_package.zip"
}
# }}}
# Set up the S3 bucket for Canvas Data archives. {{{
resource "aws_s3_bucket" "canvas_data" {
  acl           = "private"
  bucket        = var.canvas_data_s3_bucket
  force_destroy = true
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
# }}}
# Set up the S3 bucket for Athena query results. {{{
resource "aws_s3_bucket" "query_results" {
  acl           = "private"
  bucket        = var.query_results_s3_bucket
  force_destroy = true
  lifecycle_rule {
    enabled = true
    id      = "${var.query_results_s3_bucket}-lifecycle"
    # Remove old query result files.
    expiration {
      days = 10
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}
# }}}
# Set up Glue. {{{
resource "aws_glue_catalog_database" "canvas_data" {
  name = var.canvas_data_glue_catalog_database
}
# }}}
# Set up SNS. {{{
resource "aws_sns_topic" "sync" {
  name = var.canvas_data_sns_topic
}
resource "aws_sns_topic_subscription" "sync" {
  endpoint  = var.notification_email
  protocol  = "email"
  topic_arn = aws_sns_topic.sync.arn
}
# }}}
# Optionally set up Secrets Manager. {{{
module "credentials_in_secrets_manager" {
  canvas_data_api_key     = var.canvas_data_api_key
  canvas_data_api_secret  = var.canvas_data_api_secret
  canvas_data_secret_name = var.canvas_data_secret_name
  count                   = var.use_secrets_manager ? 1 : 0
  source                  = "./modules/credentials_in_secrets_manager"
}
# }}}
# Set up Lambda. {{{
# A generic assume role policy for fetch, and sync.
data "aws_iam_policy_document" "generic_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
  }
}
# Setup the IAM role for fetch. {{{
data "aws_iam_policy_document" "fetch" {
  # Provide read-write access to the canvas data S3 bucket. {{{
  statement {
    actions   = ["s3:ListBucket"]
    resources = ["${aws_s3_bucket.canvas_data.arn}"]
  }
  statement {
    actions   = ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"]
    resources = ["${aws_s3_bucket.canvas_data.arn}/*"]
  }
  # }}}
}
resource "aws_iam_role" "fetch" {
  assume_role_policy = data.aws_iam_policy_document.generic_assume_role.json
  inline_policy {
    name   = "fetch_role_policy"
    policy = data.aws_iam_policy_document.fetch.json
  }
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  name = var.canvas_data_iam_role_fetch
}
# }}}
# Setup the Lambda function for fetch. {{{
resource "aws_lambda_function" "fetch" {
  function_name = var.canvas_data_lambda_fetch_func
  handler       = "fetch-canvas-data-file.lambda_handler"
  memory_size   = 256
  role          = aws_iam_role.fetch.arn
  runtime       = "python3.9"
  s3_bucket     = aws_s3_bucket.deployment.id
  s3_key        = aws_s3_bucket_object.deployment_package.id
  timeout       = 600
}
# }}}
# Setup the IAM role for sync. {{{
data "aws_iam_policy_document" "sync_base" {
  # Provide read-write access to the canvas data S3 bucket. {{{
  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.canvas_data.arn]
  }
  statement {
    actions   = ["s3:GetObject", "s3:PutObject", "s3:DeleteObject"]
    resources = ["${aws_s3_bucket.canvas_data.arn}/*"]
  }
  # }}}
  # Provide read and limited write access to Glue resources. {{{
  statement {
    actions = ["glue:GetDatabase"]
    resources = [
      "arn:aws:glue:${local.region}:${local.account_id}:catalog",
      "arn:aws:glue:${local.region}:${local.account_id}:database/default",
    ]
  }
  statement {
    actions = ["glue:CreateTable", "glue:GetDatabase", "glue:GetTable",
    "glue:UpdateTable"]
    resources = [
      "arn:aws:glue:${local.region}:${local.account_id}:catalog",
      format("arn:aws:glue:%s:%d:database/%s", local.region,
      local.account_id, aws_glue_catalog_database.canvas_data.name),
      format("arn:aws:glue:%s:%d:table/%s/*", local.region,
      local.account_id, aws_glue_catalog_database.canvas_data.name),
    ]
  }
  # }}}
  # Provide publish access to the SNS topic. {{{
  statement {
    actions   = ["sns:Publish"]
    resources = [aws_sns_topic.sync.arn]
  }
  # }}}
  # Provide execute access to the fetch and sync functions. {{{
  statement {
    actions = ["lambda:InvokeFunction", "lambda:InvokeAsync"]
    resources = [
      format("arn:aws:lambda:%s:%d:function:%s", local.region,
      local.account_id, var.canvas_data_lambda_fetch_func),
      format("arn:aws:lambda:%s:%d:function:%s", local.region,
      local.account_id, var.canvas_data_lambda_sync_func),
    ]
  }
  # }}}
}
data "aws_iam_policy_document" "sync" {
  source_policy_documents = concat(
    # Include the base policy document.
    [data.aws_iam_policy_document.sync_base.json],
    # Optionally include the policy document for Secrets Manager.
    var.use_secrets_manager
    ? [module.credentials_in_secrets_manager[0].policy_document]
    : []
  )
}
resource "aws_iam_role" "sync" {
  assume_role_policy = data.aws_iam_policy_document.generic_assume_role.json
  inline_policy {
    name   = "sync_role_policy"
    policy = data.aws_iam_policy_document.sync.json
  }
  managed_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
  ]
  name = var.canvas_data_iam_role_sync
}
# }}}
# Setup the Lambda function for sync {{{
resource "aws_lambda_function" "sync" {
  environment {
    variables = {
      "api_sm_id" = (
        var.use_secrets_manager ?
        "${module.credentials_in_secrets_manager[0].api_sm_id}" : null
      )
      "api_key" = (
        var.use_secrets_manager ? null : "${var.canvas_data_api_key}"
      )
      "api_secret" = (
        var.use_secrets_manager ? null : "${var.canvas_data_api_secret}"
      )
      "database_name"       = "${aws_glue_catalog_database.canvas_data.name}"
      "fetch_function_name" = "${aws_lambda_function.fetch.function_name}"
      "s3_bucket"           = "${aws_s3_bucket.canvas_data.id}"
      "sns_topic"           = "${aws_sns_topic.sync.arn}"
    }
  }
  function_name = var.canvas_data_lambda_sync_func
  handler       = "sync-canvas-data-files.lambda_handler"
  memory_size   = 512
  role          = aws_iam_role.sync.arn
  runtime       = "python3.9"
  s3_bucket     = aws_s3_bucket.deployment.id
  s3_key        = aws_s3_bucket_object.deployment_package.id
  timeout       = 900
}
# }}}
# }}}
# Set up EventBridge (previously CloudWatch Events.) {{{
resource "aws_cloudwatch_event_rule" "sync" {
  name                = var.canvas_data_eventbridge_sync_trig
  schedule_expression = "cron(0 7 * * ? *)"
}
resource "aws_cloudwatch_event_target" "sync" {
  arn  = aws_lambda_function.sync.arn
  rule = aws_cloudwatch_event_rule.sync.id
}
resource "aws_lambda_permission" "sync_via_event" {
  statement_id  = var.canvas_data_lambda_perm_sync_trig
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.sync.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.sync.arn
}
# }}}
# Set up an IAM group. {{{
data "aws_iam_policy_document" "researcher" {
  # Provide read-only access to the canvas data S3 bucket. {{{
  statement {
    actions   = ["s3:ListBucket"]
    resources = [aws_s3_bucket.canvas_data.arn]
  }
  statement {
    actions   = ["s3:GetObject"]
    resources = ["${aws_s3_bucket.canvas_data.arn}/*"]
  }
  # }}}
  # Provide read-only access to Glue resources. {{{
  statement {
    actions = ["glue:GetDatabase"]
    resources = [
      "arn:aws:glue:${local.region}:${local.account_id}:catalog",
      "arn:aws:glue:${local.region}:${local.account_id}:database/default",
    ]
  }
  statement {
    actions = [
      "glue:BatchGetPartition",
      "glue:GetDatabase",
      "glue:GetDatabases",
      "glue:GetPartition",
      "glue:GetPartitions",
      "glue:GetTable",
      "glue:GetTables"
    ]
    resources = [
      "arn:aws:glue:${local.region}:${local.account_id}:catalog",
      format("arn:aws:glue:%s:%d:database/%s", local.region,
      local.account_id, aws_glue_catalog_database.canvas_data.name),
      format("arn:aws:glue:%s:%d:table/%s/*", local.region,
      local.account_id, aws_glue_catalog_database.canvas_data.name),
    ]
  }
  # }}}
  # Provide read-only access to Athena resources. {{{
  statement {
    actions = [
      "athena:BatchGetQueryExecution",
      "athena:GetDataCatalog",
      "athena:GetDatabase",
      "athena:GetQueryExecution",
      "athena:GetQueryResults",
      "athena:GetQueryResultsStream",
      "athena:GetTableMetadata",
      "athena:GetWorkGroup",
      "athena:ListDataCatalogs",
      "athena:ListDatabases",
      "athena:ListEngineVersions",
      "athena:ListQueryExecutions",
      "athena:ListTableMetadata",
      "athena:ListWorkGroups",
      "athena:StartQueryExecution",
      "athena:StopQueryExecution"
    ]
    resources = ["*"]
  }
  # }}}
  # Provide read-write access to the query results S3 bucket. {{{
  statement {
    actions = [
      "s3:AbortMultipartUpload",
      "s3:CreateBucket",
      "s3:GetBucketLocation",
      "s3:GetObject",
      "s3:ListBucket",
      "s3:ListBucketMultipartUploads",
      "s3:ListMultipartUploadParts",
      "s3:PutBucketPublicAccessBlock",
      "s3:PutObject"
    ]
    resources = [
      aws_s3_bucket.query_results.arn,
      "${aws_s3_bucket.query_results.arn}/*"
    ]
  }
  # }}}
}
resource "aws_iam_group" "researcher" {
  name = var.canvas_data_iam_group
}
resource "aws_iam_group_policy" "researcher" {
  group  = aws_iam_group.researcher.name
  name   = var.canvas_data_iam_group_policy
  policy = data.aws_iam_policy_document.researcher.json
}
# }}}
