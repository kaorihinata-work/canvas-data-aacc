#! /bin/sh

set -e
exec </dev/null

_FULLPATH="$(pwd)/deployment_package.zip"

build_deployment_package() {
    PYTHON="`which python3`"
    if [ -z "${PYTHON}" ]
    then
        # Failed to find python3.
        return 1
    fi
    git clone "https://github.com/Harvard-University-iCommons/\
canvas-data-aws.git"
    "${PYTHON}" -m venv package_venv
    package_venv/bin/pip install --upgrade pip setuptools wheel
    package_venv/bin/pip install --no-warn-conflicts \
        -r canvas-data-aws/lambda/requirements.txt
    cd package_venv/lib/python3.?*/site-packages
    zip -9r "${_FULLPATH}" . -x '*.pyc'
    cd ../../../../canvas-data-aws/lambda
    zip -9 "${_FULLPATH}" fetch-canvas-data-file.py \
        sync-canvas-data-files.py
}

if [ ! -f "${_FULLPATH}" ]
then
    build_deployment_package >&2
fi
printf '{"path": "%s"}\n' "${_FULLPATH}"
